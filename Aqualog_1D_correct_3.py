# This script processes Aqualog emission spectra and calculates mean peak values for each sample on a user-defined 
# 	emission interval. Optional: plotting, saving entire dataset, and saving flagged (unprocessed) samples
#
# INPUT: BG and sample emission spectra (raw data) for a particular excitation wavelength, Aqualog Integration Areas
# COMPUTATION: subtract BG from samples, correct for Raman area, compute peak for user-specified emission interval 
#     (the interval is inclusive by default)
# OUTPUT: corrected sample spectra in csv format
#
# NOTE: input file must be saved as Windows CSV; paths are absolute and must be modified for use on other machines

import sys
import csv
import heapq
import time
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt

# Check for correct number of args (optional 6th arg) and assign file identifier if necessary
file_identifier=""
if len(sys.argv) == 5:
	pass
elif len(sys.argv) == 6:
	file_identifier = "_" + sys.argv[5]
else:    
	sys.exit("usage: Python Aqualog_1D_correct_3.py [Aqualog Data filename] [Aqualog Integration Area filename]" 
		" [Dilution Factor filename] [peak emission interval; eg '514-525'] [OPTIONAL: output file unique identifier]")

# Declare module globals
data_filename = sys.argv[1]
IA_filename = sys.argv[2]
DF_filename = sys.argv[3]
emission_interval = sys.argv[4]
todays_date = time.strftime("%m_%d_%y")
excitation_wl = data_filename.split("_")[9]
dye_type=""
if excitation_wl == "492":
	dye_type="DTAF"
elif excitation_wl == "555":
	dye_type="RWT"


# Catch potential mismatch dye type for the emission interval
if (dye_type == "DTAF" and emission_interval != "514-525") or (dye_type == "RWT" and emission_interval != "578-585"):
	user_response_warning = raw_input("\nWARNING: Dye Type and Peak Emission Interval do not match."
		"\n(DTAF measured on 514-525 and RWT on 578-585) \nProceed? (y/n)\n")
	if user_response_warning != "y":
		sys.exit("\nProgram terminated by user - Dye Type and Emission Interval mismatch\n")

# Load dataframes (one version of full dataset with header and one without header)
df = pd.read_csv(data_filename, delimiter=',', header=None)
int_area_df = pd.read_csv(IA_filename, delimiter=',')
dilution_factors_df = pd.read_csv(DF_filename, delimiter=',')

# For each sample, find the appropriate BG dataset and subtract from sample

# Transpose the rows/columns so that each sublist stores an individual dataset 
# 	(e.g. each sublist is one column of data form the original input file)
df_list_transposed = df.T.values 
integration_areas = int_area_df.T.values
dilution_factors = dilution_factors_df.values

# At this point, df_list_transposed contains all raw values (including headers); df_list_transposed_BGs contains 
# 	just BG values (including headers)

#
# ----------------------------------------------------------------------
#
# 						COMPUTATIONS / CORRECTIONS
#

#
# Step 1A correct for dilution factors
#
dilution_corrected=[]
for column in df_list_transposed:
	for dilution_column in dilution_factors:
		if column[0] == dilution_column[0]:
			temp_column = column[:2].tolist() # convert from numpy NDarray
			column_vals = column[2:]
			dilution_val = float(dilution_column[1])
			this = [float(a)/float(dilution_val) for a in column_vals]	
			for value in this:
				temp_column.append(value)
			dilution_corrected.append(temp_column)
			break
	else:
		dilution_corrected.append(column)

# 
# Step 1B correct background and samples for Raman area
# 

# Compute mean Integration area
# 	Integration areas is 2D list where [0] is date and [1] is integration area
df_list_transposed_int_area_corrected=[]

mean_int_area = sum(integration_areas[1])/float(len(integration_areas[1]))


for col_index, column in enumerate(dilution_corrected):
	# Check for and store emission wavelength column (leftmost column in spreadsheet)
	if col_index == 0:
		df_list_transposed_int_area_corrected.append(column) 
	# Otherwise proceed to sample columns
	else:
		sample_date = column[1]
		temp_column=[]
		for index, date in enumerate(integration_areas[0]):
			if sample_date == date:
				int_area = float(integration_areas[1][index])
				#print "integration area: " + str(int_area)
				for ind, val in enumerate(column):
					if ind <= 1:
						temp_column.append(val)
					elif ind > 1:
						temp_column.append(float(val)/int_area)
				#print " sample_ID " + column[0] + " int_area: " + str(int_area)
				break
		else:
			for ind, val in enumerate(column):
				if ind <= 1:
					temp_column.append(val)
				elif ind > 1:
					temp_column.append(float(val)/mean_int_area)
		df_list_transposed_int_area_corrected.append(temp_column)


#
# Step 2 subtract BG from sample		
#

# Assemble 2d list of BGs only
df_list_transposed_BGs=[]
for column in df_list_transposed_int_area_corrected:
	name = column[0]
	if "BG" in name:
		if "JUNE" in name:
			name = name[:10]
		else:
			name = name[:5]
		
		flat_temp_column = [name] + column[1:]
		df_list_transposed_BGs.append(flat_temp_column)

number_of_matches=0
flagged_samples=[]
df_list_transposed_int_area_corrected_bg_subtracted = [df_list_transposed_int_area_corrected[0]]

#
# Create lists to store avg BG for C1, C2, and N1 for purpose of subtracting from 'E' runs 
#	that do not have an associated BG 
#
column_length = len(df_list_transposed_int_area_corrected[0])-2
C1_BG_avg, C2_BG_avg, N1_BG_avg = [0]*column_length, [0]*column_length, [0]*column_length

C1_index, C2_index, N1_index = 0.,0.,0.
# Calculate avg BG value for c1, c2, n1
for column in df_list_transposed_int_area_corrected:
	if "C1" in column[0] and "BG" in column[0]:
		C1_BG_avg = [float(a)+float(b) for a,b in zip(C1_BG_avg, column[2:])]
		C1_index += 1.
	elif "C2" in column[0] and "BG" in column[0]:
		C2_BG_avg = [float(a)+float(b) for a,b in zip(C2_BG_avg, column[2:])]
		C2_index += 1.
	elif "N1" in column[0] and "BG" in column[0]:
		N1_BG_avg = [float(a)+float(b) for a,b in zip(N1_BG_avg, column[2:])]
		N1_index += 1.

# Divide the total by the number of background runs to obtain average background
C1_BG_avg = [float(x/C1_index) for x in C1_BG_avg]
C2_BG_avg = [float(x/C2_index) for x in C2_BG_avg]
N1_BG_avg = [float(x/N1_index) for x in N1_BG_avg]

#
# Ambient: determine whether to subtract ambient from runs with lots of negative values
#
apply_ambient=False
user_response_ambient = raw_input("\nSubtract Ambient (instead of associated BG) on all runs? (y/n)\n")
if user_response_ambient == "y":
	apply_ambient=True
	file_identifier = "_ambient" + file_identifier
	
ambient_c1=[]
ambient_c2=[]
ambient_n1=[]
for column in df_list_transposed_int_area_corrected:
	if "Ambient C1" in column[0]:
		ambient_c1=column[2:]
	if "Ambient C2" in column[0]:
		ambient_c2=column[2:]
	if "Ambient N1" in column[0]:
		ambient_n1=column[2:]

# Iterate through columns again and perform BG subtraction
for column in df_list_transposed_int_area_corrected:
	flagged_sample = True
	sample_name = column[0]
	for bg_column in df_list_transposed_BGs:
		bg_name = bg_column[0]
			
		# Special case for ambients
		if apply_ambient: 
			if "BG" in sample_name: # 9/1/17 update
				df_list_transposed_int_area_corrected_bg_subtracted.append(column)
				flagged_sample=False
				number_of_matches+=1
				break
			elif "Ambient" in sample_name:
				flagged_sample=False
				break
			elif "C1FD35:30" in sample_name or "N2FDB8:00" in sample_name: # improperly formatted samples
				flagged_sample=True
			else:
				col_header = column[:2]
				column_vals = column[2:]
				flagged_sample=False
				# Assign "background" values, in this case Ambient, according to C1/C2/N1 runs
				if "C1" in sample_name:
					bg_vals = ambient_c1
				elif "C2" in sample_name:
					bg_vals = ambient_c2
				elif "N1" in sample_name:
					bg_vals = ambient_n1
				else:
					flagged_sample=True
				# Subtract Ambient from sample if not flagged
				if not flagged_sample:
					flat_temp_column = col_header + [float(a)-float(b) for a,b in zip(column_vals, bg_vals)]
					df_list_transposed_int_area_corrected_bg_subtracted.append(flat_temp_column)
					number_of_matches+=1
				break

		elif "Ambient" in sample_name: # 9/5/17 update
			flagged_sample=False
			break

		elif bg_name in sample_name:
			# Check to prevent against subtracting BG from itself
			if "BG" in sample_name: # 9/1/17 update
				df_list_transposed_int_area_corrected_bg_subtracted.append(column)
				flagged_sample=False
				number_of_matches+=1
				break
			# Check that April samples aren't double-matching with both June and April BG files 
			# 	since April sample names don't have month indicated
			elif ("JUNE" in bg_name and "JUNE" in sample_name) or (not "JUNE" in bg_name and not "JUNE" in sample_name):
				col_header = column[:2]
				column_vals = column[2:]
				bg_vals = bg_column[2:]
				
				# Subtract BG from sample
				flat_temp_column = col_header + [float(a)-float(b) for a,b in zip(column_vals, bg_vals)]
				
				df_list_transposed_int_area_corrected_bg_subtracted.append(flat_temp_column)
				flagged_sample=False
				number_of_matches+=1
				break
		
		# Special case for 'E' samples and (as of 09/18/17) 'I' samples
		elif "FE" in sample_name or "FI" in sample_name:
			col_header = column[:2] 
			column_vals = column[2:]
			# Assign bg_vals based on whether it's c1, c2 or n1:
			if "C1" in sample_name:
				bg_vals = C1_BG_avg
			elif "C2" in sample_name:
				bg_vals = C2_BG_avg
			else:
				bg_vals = N1_BG_avg
			# Subtract BG from sample
			flat_temp_column = col_header + [float(a)-float(b) for a,b in zip(column_vals, bg_vals)]
			
			df_list_transposed_int_area_corrected_bg_subtracted.append(flat_temp_column)
			flagged_sample=False
			number_of_matches+=1
			break

	# Flag any samples that don't find a match (currently there are ~10 of them)
	if flagged_sample:
		flagged_samples.append(sample_name)		

print "number of matches: " + str(number_of_matches) 
print "number of flagged samples: " + str(len(flagged_samples))


# Step 3 find five highest values on a certain wavelength range
# 
# 8/28/17 update: interval is closed instead of "open" 
# previously allowed 1 point outside the range on each end of the interval was included

lower_emission_bound = emission_interval.split('-')[0]
upper_emission_bound = emission_interval.split('-')[1]
upper_emission_index=0
lower_emission_index=0

wavelengths = df_list_transposed_int_area_corrected_bg_subtracted[0]

for index, val in enumerate(wavelengths):
	if wavelengths[index-1] < lower_emission_bound:
		lower_emission_index = index
	if val < upper_emission_bound:
		upper_emission_index = index

print "lower: " + str(lower_emission_bound)
print "upper: " + str(upper_emission_bound)
print "lower_index: " + str(lower_emission_index)
print "upper_index: " + str(upper_emission_index)

final_data=[]
# Iterate through corrected samples and find peaks on given range
for col_index, column in enumerate(df_list_transposed_int_area_corrected_bg_subtracted):
	if col_index != 0:
		applicable_vals = column[lower_emission_index:upper_emission_index+1]
		
		# if column[0] == "N1FDB10:00":
		# 	print applicable_vals
		applicable_vals.sort()
		#print applicable_vals
		top5_peaks = applicable_vals[-5:]
		# if column[0] == "JUNE C2FDG30:00":
		# 	print top5_peaks
		mean = sum(top5_peaks)/float(len(top5_peaks))
		# if column[0] == "JUNE C2FDG30:00":
		# 	print "mean: " + str(mean)
		std_err_mean = stats.sem(top5_peaks)
		
		
		# add "0" before times in single-digits to facilitate sorting in excel:
		new_sample_name=column[0]
		new_sample_time="n/a"
		if "BG" not in new_sample_name:
			sample_ID=""
			sample_time=""
			new_sample_minutes=""
			new_sample_seconds=""
			if "JUNE" in column[0]:
				sample_ID = "JUNE_" + column[0][5:10]
				sample_time = column[0][10:]
			elif "FE" in column[0] or "FI" in column[0]:
				sample_ID = column[0][:4]
				sample_time = column[0][4:]
			else:
				sample_ID = column[0][:5]
				sample_time = column[0][5:]
			sample_minutes = sample_time.split(":")[0] # pull out minutes (C2FDB6:30 -> 6)
			sample_seconds = sample_time.split(":")[1][:2]
			
			#convert sample from units of seconds to units of minutes
			if sample_seconds == "30":
				new_sample_seconds=".5"
			else:
				new_sample_seconds=".0"
			# add "0" before times in single-digits
			if len(sample_minutes) == 1:
				new_sample_minutes = "0" + sample_minutes
			else:
				new_sample_minutes = sample_minutes

			new_sample_time = new_sample_minutes + new_sample_seconds
			new_sample_name = sample_ID + " " + new_sample_minutes + ":" + sample_seconds

		final_data.append([new_sample_name, mean, std_err_mean, new_sample_time])


final_data.sort(key=lambda x: x[0], reverse=False)


#
# ----------------------------------------------------------------------
#
# 							OUTPUT
#

outfile_name = "/Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_outputs/" + dye_type + "_peaks_" + emission_interval + "_" + todays_date + file_identifier + ".csv"
with open(outfile_name, 'wb') as myfile:
	wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
	wr.writerow(["Sample ID", "Mean of 5 peak values", "Std Error Mean", "Time"])
	for row in final_data:
		if "BG" not in row[0]:
			wr.writerow(row)

print "Writing processed data to " + outfile_name

#
# ----------------------------------------------------------------------
#
# 							OPTIONAL EXTRAS (user-determined)
#


# OPTIONAL: save flagged samples
user_response = raw_input("Save flagged samples? (y/n) \n")

if user_response == "y":
	outfile_name = "/Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_outputs/" + dye_type + "_flagged_samples_" + emission_interval + "_" + todays_date + file_identifier + ".csv"
	with open(outfile_name, 'wb') as myfile:
		wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
		for row in flagged_samples:
			wr.writerow([row])
	print "saved flagged samples"

# OPTIONAL: save all corrected data to csv
user_response2 = raw_input("Save all corrected data? (y/n) \n")

if user_response2 == "y":
	outfile_name = "/Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_outputs/" + dye_type + "_all_data_" + emission_interval + "_" + todays_date + file_identifier + ".csv"
	# Transpose 2d array:
	all_final_data = pd.DataFrame(df_list_transposed_int_area_corrected_bg_subtracted)
	final_data_transposed = all_final_data.T.values
	with open(outfile_name, 'wb') as myfile:
		wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
		for row in final_data_transposed:
			wr.writerow(row)
	print "saved all corrected data"

# OPTIONAL: plot max fluorescence vs time for each depth:

# Helper function for plotting all data or subsets with a certain sample name constraint
def scatter_plotter(all_plots, positive_constraint, negative_constraint, titles, axis, figure, graph_name):
	number_passed=0
	for index, plot_values in enumerate(all_plots):
		#assign correct colors to subplots
		if positive_constraint in titles[index] and negative_constraint not in titles[index]:
			color = title[index][-1]
			if color == "W":
				color="grey"
			elif color == "P":
				color="purple"
			elif color == "a":
				color="w"
			elif color == "E":
				color="orange"
			
			# make scatterplot
			axis[index-number_passed].scatter(plot_values[0], plot_values[1], c=color, marker='.')
			# customize
			axis[index-number_passed].set_title(str(title[index]), size=6)	# set title
			plt.tick_params(axis='both', which='major', labelsize=6) # set tick label size
			# set x and y range
			axis[index-number_passed].set_xlim([-2, 62])
			# customize y axis on some plots
			# DTAF 
			if "N1FDG" in title[index] and dye_type == "DTAF":
				axs[index].set_ylim([-.12, .1])
			elif ("N1FDP" in title[index] or "N1FDR" in title[index]) and dye_type == "DTAF":
				axs[index].set_ylim([-.05,.075])
			# RWT

			# *** DO NOT DELETE the block below

			# elif ("C2FDB" in title[index] or "C2FDP" in title[index] or "C2FDW" in title[index] 
			# 		or "C2FDY" in title[index]) and dye_type == "RWT":
			# 	axs[index].set_ylim([-.2, 1.2])
			# elif ("N1FDR" in title[index] or "N1FDP" in title[index]) and dye_type == "RWT":
			# 	axs[index].set_ylim([-.05, 0.075])
			# elif "N1FDG" in title[index] and dye_type == "RWT":
			# 	axs[index].set_ylim([-.1, 0.0])
			# else:
			# 	axs[index].set_autoscaley_on(True)

			# *** DO NOT DELETE the block above
			
			# set vertical and horizontal axis titles
			figure.text(0.5, 0.04, 'Time (min)', ha='center', va='center')
			figure.text(0.06, 0.5, 'Peak Fluorescence', ha='center', va='center', rotation='vertical')
		else:
			number_passed+=1

	figure.suptitle(graph_name, fontsize=14)
	plt.savefig('/Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_outputs/' + dye_type 
		+ '_output_figures/' + graph_name + "_" + todays_date + file_identifier + '.png')
	plt.close(figure)
	print "Done with plot"

# Add null values for those depths/months with no other data to facilitate ease of automatic plotting in grid format; 
# 	this way a plot for these values will populate, ensuring that other plots in the correct cell in the grid
final_data.append(['C1FDB_no_data',0,0,0])
final_data.append(['C1FDR_no_data',0,0,0])
final_data.append(['JUNE_C1FE_no_data',0,0,0])
final_data.append(['JUNE_C2FE_no_data',0,0,0])
final_data.sort(key=lambda x: x[0], reverse=False)

# organize data in new list format for plotting

plots=[] 	# plots is "3d array" (er, list of list of lists) where outer index is the plot, middle index is 
			# 	the depth, inner index are peak values at that depth
temp_plot_times=[] # temporary list to store times for each depth
temp_plot_values=[] # temp list to store peak fluorescence values for each depth
title=[]
index=0
set_ID=final_data[index][0].split(" ")[0]
while index < len(final_data)-1: # loop through entire final_data list
	if not "BG" in final_data[index][0]:
		current_ID=final_data[index][0].split(" ")[0]
		if set_ID == current_ID:
			temp_plot_values.append(final_data[index][1])
			temp_plot_times.append(final_data[index][3])
		else:
			plots.append([temp_plot_times, temp_plot_values]) #append all samples in the depth to list of plots
			#title.append(final_data[index-1][0].split(" ")[0])
			title.append(set_ID)
			temp_plot_values=[] # reset temporary list
			temp_plot_times=[] # reset temporary list
			temp_plot_values.append(final_data[index][1])
			temp_plot_times.append(final_data[index][3])
			set_ID = final_data[index][0].split(" ")[0] # reset set_ID 
	index+=1
#append last group of values
plots.append([temp_plot_times, temp_plot_values]) #append all samples in the depth to list of plots
title.append(set_ID)

# plot all data (organized by Month, Depth, and Plant Type)
user_response3 = raw_input("Create overview plots? (y/n) \n")
if user_response3 == "y":
	# plot fluorescence vs time:

	# adjust font size
	plt.rcParams.update({'font.size': 6})

#Figure 1 - All
	fig, axs = plt.subplots(5,7, figsize=(15, 7), facecolor='w', edgecolor='k')
	fig.subplots_adjust(hspace = .5, wspace=.5)
	axs = axs.ravel()
	# scatter_plotter(all_plots, positive_constraint, negative_constraint, titles, axis, figure):
	scatter_plotter(plots, "", "***", title, axs, fig, "Peak Fluorescence vs Time (min) - " + dye_type + " All")
	
#Figure 2 - only colocasia
						# B 	# G 	# P 	# R 	# W 	# Y 	# FE
	# row 1: April C1 			x 		x 				x 		x 		x
	# row 2: June  C1 	x 		x 		x 		x 		x 		x
	# row 3: April C2 	x 		x 		x 		x 		x  		x 		x
	# row 4: June  C2 	x 		x 		x 		x 		x 		x

	fig2, axs2 = plt.subplots(4,7, figsize=(15, 6), facecolor='w', edgecolor='k')
	fig2.subplots_adjust(hspace = .5, wspace=.5)
	axs2 = axs2.ravel()
	scatter_plotter(plots, "C", "***", title, axs2, fig2, "Peak Fluorescence vs Time (min) - " + dye_type + " Colocasia")

#Figure 3 - only nelumbo
						# B 	# G 	# P 	# R 	# W 	# Y 	# FE
	# row 1: April N1 	x		x		x		x	    x 		x 		x

	fig3, axs3 = plt.subplots(1,7, figsize=(15, 3), facecolor='w', edgecolor='k')
	fig3.subplots_adjust(hspace = .5, wspace=.5)
	axs3 = axs3.ravel()
	scatter_plotter(plots, "N", "JUNE", title, axs3, fig3, "Peak Fluorescence vs Time (min) - " + dye_type + " Nelumbo")

#Figure 4 - only April
	fig4, axs4 = plt.subplots(3,7, figsize=(15, 5), facecolor='w', edgecolor='k')
	fig4.subplots_adjust(hspace = .5, wspace=.5)
	axs4 = axs4.ravel()
	scatter_plotter(plots, "", "JUNE", title, axs4, fig4, "Peak Fluorescence vs Time (min) - " + dye_type + " April")

#Figure 5 - only June
	fig5, axs5 = plt.subplots(2,7, figsize=(15, 4), facecolor='w', edgecolor='k')
	fig5.subplots_adjust(hspace = .5, wspace=.5)
	axs5 = axs5.ravel()
	scatter_plotter(plots, "JUNE", "***", title, axs5, fig5, "Peak Fluorescence vs Time (min) - " + dye_type + " June")

# individual plots, save only and don't display since there are so many
user_response4 = raw_input("Create individual plots? (y/n) \n")

if user_response4 == "y":
	plt.rcParams.update({'font.size': 12}) # adjust font size
	for index, plot_values in enumerate(plots):
		if "no_data" not in title[index]:
			#assign correct colors to subplots
			plt.figure(figsize=(8, 6), dpi=300, facecolor='w', edgecolor='k')
			color = title[index][-1]
			if color == "W":
				color = "grey"
			elif color == "P":
				color = "purple"
			elif color == "E":
				color = "orange"

			plt.scatter(plot_values[0], plot_values[1], c=color, marker='.')
			
			plt.title(str(title[index]), size=14)	# title
			
			#set x range
			plt.xlim([-2, 62])

			#for certain figures with outliers, change y limits as well
			if "N1FDG" in title[index] and dye_type == "DTAF":
				plt.ylim([-.12,.1])
			elif ("N1FDP" in title[index] or "N1FDR" in title[index]) and dye_type == "DTAF":
				plt.ylim([-.05,.075])

			plt.xlabel("Time (min)")
			plt.ylabel("Peak Fluorescence")
			#plt.ylim([0, .25])
			plt.savefig('/Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_outputs/' + dye_type 
				+ '_output_figures/individual_plots/' + str(title[index]) + "_" + todays_date + file_identifier + '.png')
			plt.close()
			print "Done with plot " + str(index+1) + " of " + str(len(plots)) 
		else:
			print "Skipping  plot " + str(index+1) + " of " + str(len(plots)) + " - contains no data"

# plot N1FDW BG, June C1FDW vs N1 ambient
# fig12 = plt.figure()
# plt.scatter()

print "DONE ALL"
# END OF SCRIPT