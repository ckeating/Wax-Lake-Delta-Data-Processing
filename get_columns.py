# Author: Colin Keating

# 8/17/17

# extracts columns with a particular string in the column header and stores those in new csv file
# NOTE: input file (default: waxlakedelta492_8_16_17_492.csv) must be in same directory as this 
#	program and must be saved as CSV

import csv
column_header = raw_input("enter the column header, for example 'C2FDP': ")
with open('/Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/waxlakedelta492_8_23_17_492.csv') as csvFile:
    reader = csv.reader(csvFile)
    column_indices=[]
    column_names=[]
    all_data=[]
    first_row=[]
    for row in reader:
    	if row[0]=="Emission wavelength":
    		first_row=row
    		column_names.append('emission wavelength')
    		for index, item in enumerate(first_row):
    			if column_header in item:
    				column_indices.append(index)
    				column_names.append(item)

    	elif row[0]!="Date run":
		   	rowdata=[]
		   	rowdata.append(row[0])
		   	for index, item in enumerate(row):
		   		if index in column_indices:
		   			#print "item: " + str(item)
		   			rowdata.append(item)
		   	#print rowdata
		   	all_data.append(rowdata)


outfile_name = column_header + ".csv"
#write applicable columns to csv
with open(outfile_name, 'wb') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    
    wr.writerow(column_names)
    for row in all_data:
    	wr.writerow(row)

print "number of columns: " + str(len(column_names)-1)
print "DONE"

