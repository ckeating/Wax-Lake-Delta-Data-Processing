# Author: Colin Keating

# 8/17/17

# INPUT: BG and sample emission spectra (raw data) for a particular excitation wavelength, Aqualog Integration Areas
# COMPUTATION: subtract BG from samples, correct for Raman area, compute peak for user-specified emission interval 
#  note: the interval is inclusive by default
# OUTPUT: corrected sample spectra in csv format

# NOTE: input file must be in same directory as this program and must be saved as Windows CSV

import sys, csv, heapq
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
# check for correct number of args
if len(sys.argv)!=5:
    sys.exit("usage: Python Aqualog_1D_correct_2.py [Aqualog Data filename] [Aqualog Integration Area filename] [Dilution Factor filename] [peak emission interval; eg '514-525']")

#get user inputs
data_filename=sys.argv[1]
IA_filename=sys.argv[2]
DF_filename=sys.argv[3]
emission_interval=sys.argv[4]

print "data_filename: " + data_filename
print "IA_filename: " + IA_filename
print "emission_interval: " + emission_interval

#load dataframes (one version of full dataset with header and one without header)
df = pd.read_csv(data_filename, delimiter=',', header=None)
int_area_df = pd.read_csv(IA_filename, delimiter=',')
dilution_factors_df = pd.read_csv(DF_filename, delimiter=',')

# for each sample, find the appropriate BG dataset and subtract from sample

df_list_transposed = df.T.values # transpose the rows/columns so that each sublist stores an individual dataset (e.g. each sublist is one column of data form the original input file)
integration_areas = int_area_df.T.values
dilution_factors = dilution_factors_df.values

# at this point, df_list_transposed contains all raw values (including headers); df_list_transposed_BGs contains 
# 	just BG values (including headers)

#
# ----------------------------------------------------------------------
#
# 						COMPUTATIONS / CORRECTIONS
#

#
# Step 1A correct for dilution factors
#
dilution_corrected=[]
for column in df_list_transposed:
	for dilution_column in dilution_factors:
		if column[0]==dilution_column[0]:
			temp_column = column[:2].tolist() # convert from numpy NDarray
			column_vals = column[2:]
			dilution_val = float(dilution_column[1])
			this = [float(a)/float(dilution_val) for a in column_vals]	
			for value in this:
				temp_column.append(value)
			dilution_corrected.append(temp_column)
			break
	else:
		dilution_corrected.append(column)

#
# Step 1B correct background and samples for Raman area
#

# compute mean Integration area
df_list_transposed_int_area_corrected=[]

mean_int_area = sum(integration_areas[1])/float(len(integration_areas[1]))

#print dilution_corrected[1]

for col_index, column in enumerate(dilution_corrected):
	if col_index == 0:
		df_list_transposed_int_area_corrected.append(column) # store emission wavelength column
	else:
		sample_date = column[1]
		temp_column =[]
		for index, date in enumerate(integration_areas[0]):
			if sample_date == date:
				int_area = float(integration_areas[1][index])
				#print "integration area: " + str(int_area)
				for ind, val in enumerate(column):
					if ind<=1:
						temp_column.append(val)
					elif ind>1:
						temp_column.append(float(val)/int_area)
				#print " sample_ID " + column[0] + " int_area: " + str(int_area)
				break
		else:
			for ind, val in enumerate(column):
				if ind<=1:
					temp_column.append(val)
				elif ind>1:
					temp_column.append(float(val)/mean_int_area)
			#print " sample_ID " + column[0] + " int_area: " + str(mean_int_area)
		df_list_transposed_int_area_corrected.append(temp_column)

print "length df_int area corrected: " + str(len(df_list_transposed_int_area_corrected))
#print "df_list_transposed_int_area_corrected[771]: " + str(df_list_transposed_int_area_corrected[771])

# ^ Initial testing Int area correction is complete

#
# Step 2 subtract BG from sample		
#

#assemble 2d list of BGs only
df_list_transposed_BGs=[]
for column in df_list_transposed_int_area_corrected:
	name = column[0]
	if "BG" in name:
		if "JUNE" in name:
			name = name[:10]
		else:
			name = name[:5]
		
		flat_temp_column = [name] + column[1:]
		df_list_transposed_BGs.append(flat_temp_column)

number_of_matches=0
flagged_samples=[]
df_list_transposed_int_area_corrected_bg_subtracted = [df_list_transposed_int_area_corrected[0]]

for column in df_list_transposed_int_area_corrected:
	flagged_sample = True
	sample_name = column[0]
	for bg_column in df_list_transposed_BGs:
		bg_name = bg_column[0]
		if bg_name in sample_name:
			# check that April samples aren't double-matching with both June and April bg files since April sample names don't have month indicated
			if ("JUNE" in bg_name and "JUNE" in sample_name) or (not "JUNE" in bg_name and not "JUNE" in sample_name):
				
				temp_column = column[:2]
				column_vals = column[2:] # working
				bg_vals = bg_column[2:]
				
				# subtract BG from sample
				flat_temp_column = temp_column + [float(a)-float(b) for a,b in zip(column_vals, bg_vals)]
				
				df_list_transposed_int_area_corrected_bg_subtracted.append(flat_temp_column)
				flagged_sample=False
				number_of_matches+=1
				break
	#flag samples that don't find a match
	if flagged_sample:
		#if not "N1FDP" in sample_name and not "C2FDG" in sample_name:
		flagged_samples.append(sample_name)		

print "number of matches: " + str(number_of_matches) #currently finds 1269 matches and 127 flagged samples (31 if not including N1FDP and C2FDG (April)) 
#print flagged_samples
print "number of flagged samples: " + str(len(flagged_samples))

# ------------ TEST passes for subtracting --------------
# for ind, val in enumerate(df_list_transposed_int_area_corrected_bg_subtracted):
# 	if df_list_transposed_int_area_corrected_bg_subtracted[ind][0]=="JUNE C2FDG30:00":
# 		print df_list_transposed_int_area_corrected_bg_subtracted[ind]

#
# Step 3 find five highest values on a certain wavelength range
# 
# 8/28/17 update: interval is closed instead of "open" 
# previously allowed 1 point outside the range on each end of the interval was included

lower_emission_bound = emission_interval.split('-')[0]
upper_emission_bound = emission_interval.split('-')[1]
upper_emission_index=0
lower_emission_index=0

wavelengths=df_list_transposed_int_area_corrected_bg_subtracted[0]

for index, val in enumerate(wavelengths):
	if wavelengths[index-1] < lower_emission_bound:
		lower_emission_index=index
	if val < upper_emission_bound:
		upper_emission_index=index

print "lower: " + str(lower_emission_bound)
print "upper: " + str(upper_emission_bound)
print "lower_index: " + str(lower_emission_index)
print "upper_index: " + str(upper_emission_index)

final_data=[]
# final_data.append(["_Sample ID", "Mean of 5 peak values", "Std Error Mean", "Time"])
#iterate through corrected samples and find peaks on given range
for col_index, column in enumerate(df_list_transposed_int_area_corrected_bg_subtracted):
	if col_index is not 0:
		applicable_vals = column[lower_emission_index:upper_emission_index+1]
		
		# if column[0] == "N1FDB10:00":
		# 	print applicable_vals
		applicable_vals.sort()
		#print applicable_vals
		top5_peaks = applicable_vals[-5:]
		# if column[0] == "JUNE C2FDG30:00":
		# 	print top5_peaks
		mean = sum(top5_peaks)/float(len(top5_peaks))
		# if column[0] == "JUNE C2FDG30:00":
		# 	print "mean: " + str(mean)
		std_err_mean = stats.sem(top5_peaks)


		# add "0" before times in single-digits to facilitate sorting in excel:
		new_sample_name=column[0]
		new_sample_time="n/a"
		if "BG" not in new_sample_name:
			sample_ID=""
			sample_time=""
			new_sample_minutes=""
			new_sample_seconds=""
			if "JUNE" in column[0]:
				sample_ID = "JUNE_" + column[0][5:10]
				sample_time = column[0][10:]
			else:
				sample_ID = column[0][:5]
				sample_time = column[0][5:]
			sample_minutes = sample_time.split(":")[0] # pull out minutes (C2FDB6:30 -> 6)
			sample_seconds = sample_time.split(":")[1][:2]
			
			#convert sample from units of seconds to units of minutes
			if sample_seconds=="30":
				new_sample_seconds=".5"
			else:
				new_sample_seconds=".0"
			# add "0" before times in single-digits
			if len(sample_minutes) == 1:
				new_sample_minutes = "0" + sample_minutes
			else:
				new_sample_minutes = sample_minutes

			new_sample_time = new_sample_minutes + new_sample_seconds
			new_sample_name = sample_ID + " " + new_sample_minutes + ":" + sample_seconds

		final_data.append([new_sample_name, mean, std_err_mean, new_sample_time])

final_data.sort(key=lambda x: x[0], reverse=False)

#
# ----------------------------------------------------------------------
#
# 							OUTPUT
#


excitation_wl = data_filename.split("_")[4]
dye_type = "DTAF"
if excitation_wl=="555":
	dye_type="RWT"

outfile_name = dye_type + "_peaks_" + emission_interval + "_dilution_corrected_8_28_17.csv"
with open(outfile_name, 'wb') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(["Sample ID", "Mean of 5 peak values", "Std Error Mean", "Time"])
    for row in final_data:
    	if "BG" not in row[0]:
    		wr.writerow(row)

#
# ----------------------------------------------------------------------
#
# 							OPTIONAL EXTRAS (user-determined)
#


# OPTIONAL: save flagged samples
user_response = raw_input("Save flagged samples? (y/n) \n")

if user_response=="y":
	outfile_name = dye_type + "_flagged_samples_" + emission_interval + "_dilution_corrected_8_28_17.csv"
	with open(outfile_name, 'wb') as myfile:
	    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
	    for row in flagged_samples:
	    	wr.writerow([row])

# OPTIONAL: save all corrected data to csv
user_response2 = raw_input("Save all corrected data? (y/n) \n")

if user_response2=="y":
	outfile_name = dye_type + "_all_data_" + emission_interval + "_dilution_corrected_8_28_17.csv"
	#transpose 2d array:
	all_final_data = pd.DataFrame(df_list_transposed_int_area_corrected_bg_subtracted)
	final_data_transposed = all_final_data.T.values
	with open(outfile_name, 'wb') as myfile:
	    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
	    for row in final_data_transposed:
    		wr.writerow(row)

# OPTIONAL: plot max fluorescence vs time for each depth:

# ***************** debugging in progress *************************
user_response3 = raw_input("Create plots for each dataset? (y/n) \n")

if user_response3=="y":
	plots=[] 	# plots is "3d array" (er, list of list of lists) where outer index is the plot, middle index is the depth, inner index are peak values at that depth
	temp_plot_times=[] # temporary list to store times for each depth
	temp_plot_values=[] # temp list to store peak fluorescence values for each depth
	title=[]
	index=0
	set_ID=final_data[index][0].split(" ")[0]
	while index < len(final_data)-1: # loop through entire final_data list
		if not "BG" in final_data[index][0]:
			current=final_data[index][0].split(" ")[0]
			if set_ID == current:
				temp_plot_values.append(final_data[index][1])
				temp_plot_times.append(final_data[index][3])
			else:
				plots.append([temp_plot_times, temp_plot_values]) #append all samples in the depth to list of plots
				title.append(current)
				temp_plot_values=[] # reset temporary list
				temp_plot_times=[] # reset temporary list
				temp_plot_values.append(final_data[index][1])
				temp_plot_times.append(final_data[index][3])
				set_ID = final_data[index][0].split(" ")[0] # reset set_ID 
		index+=1
		

	# plot fluorescence vs time:

	# adjust font size
	plt.rcParams.update({'font.size': 6})

	fig, axs = plt.subplots(4,7, figsize=(15, 6), facecolor='w', edgecolor='k')
	fig.subplots_adjust(hspace = .5, wspace=.5)

	axs = axs.ravel()

	for index, plot_values in enumerate(plots):
		#assign correct colors to subplots
		color = title[index][-1]
		if color=="W":
			color="grey"
		elif color=="P":
			color="purple"

		axs[index].plot(plot_values[0], plot_values[1], color)
		
		axs[index].set_title(str(title[index]), size=6)	# title
		plt.tick_params(axis='both', which='major', labelsize=6)

		#set x and y range
		axs[index].set_xlim([0, 60])
		#axs[index].set_ylim([0, .25])
	
	fig.suptitle("Peak Fluorescence vs Time (min)", fontsize=14)
	plt.show()

print "\nDONE"
# END OF SCRIPT