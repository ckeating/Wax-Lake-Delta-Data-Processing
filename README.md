Author: Colin Keating  
Lab Manager, Environmental Systems Dynamics Laboratory  
PI: Laurel Larsen  
Department of Geography  
University of California, Berkeley  

Last updated: 9/27/17

## PURPOSE  
To process spectrofluorometry data acquired from an Aqualog Spectrofluorometer (Horiba Scientific) associated with field work at Wax Lake Delta, Louisiana.


## EXAMPLE USAGE  
	cd /Users/Colin/Documents/Colin/Work/Berkeley/WLD_data_processing_scripts
	Python Aqualog_1D_correct_3.py /Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_inputs/waxlakedelta492_8_16_17_492_cleaned.csv /Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_inputs/Aqualog_Integration_Areas.csv /Users/Colin/Documents/Colin/Work/Berkeley/WLD_plots/Aqualog_1D_correct_3_inputs/Dilution_factors.csv 514-525


## INPUTS  
1. background_and_sample_emission_spectra  
  * cleaned raw data compiled in Excel from multiple Aqualog experiments], CSV-format  
2. aqualog_integration_areas  
  * CSV-format  
3. dilution_factors  
  * CSV-format  
4. emission_interval  
  * units of nm, example syntax: 514-525  
5. output_file_identifier  
  * e.g. today's date  


## COMPUTATIONAL STEPS  
Subtract background emission spectrum from samples, correct for Raman area, correct for dilution factor, compute peak (mean of top 5 largest peak values) for user-specified emission interval.  The user-specified interval is exclusive by default; only emission peak values within the range will be considered.


## OUTPUT  
CSV file containing sample name, mean peak values on user-specified emission range, standard deviation of the mean, and time.  

Optional additional outputs (user is prompted withh options in command shell during program execution):
1. Save flagged samples that were not processed (due to no associated background spectrum), in CSV-format.

2. Save all corrected spectra for all individual timepoints to CSV; this file will be approximately the same as the input dataset but does not include flagged samples, since those samples cannot be background-subtracted. 

3. Output 5 overview figures containing subplots of:  
  * All depths/runs   
  * only colocasia  
  * only nelumbo  
  * only April  
  * only June  

4. Output larger individual plots of individual sample runs/depths  

## NOTES  
To facilitate graphing, empty datasets for several depths were added as placeholders for missing data from the original experiment.  They are marked in the overview plots with the sample name followed by "no_data"  These were added following all data correction/computational steps and these datasets are not present in any .csv output files, nor do they affect the data in these files in any way.  

'FE' and 'FI' runs had no associated BG and were processed by subtracting the mean BG for C1/C2/N1 as applicable.  

In a previous software version prior to 9/6/17, DTAF runs June_C1FDW and N1FDW could be potionally run against ambient samples. Thus output and graphical data labeled prior to 9/6/17 may be "incorrect" based on the assumption that all samples were run against BG or Ambient.  For research purposes, refer only to outputs dated 9/6/17 or later.  

Note that BG and ambient data are very similar and ambient subtraction doesn't lead to a large graphical difference in some instances.  See Overview-All plots for comparison of Ambient and BG (open simultaneously in Preview in order to toggle back and forth and see deviations).  