# Author: Colin Keating

# 8/17/17

# INPUT: BG and sample emission spectra (raw data) for a particular excitation wavelength, Aqualog Integration Areas
# COMPUTATION: subtract BG from samples, correct for Raman area, compute peak for user-specified emission interval 
# OUTPUT: corrected sample spectra in csv format

# NOTE: input file must be in same directory as this program and must be saved as Windows CSV

import sys, csv
import pandas as pd
# check for correct number of args
if len(sys.argv)!=4:
    sys.exit("usage: Python Aqualog_1D_correct.py [aqualog data filename] [Aqualog Integration Area filename] [peak emission interval; eg '514-525']")

#get user inputs
data_filename=sys.argv[1]
IA_filename=sys.argv[2]
emission_interval=sys.argv[3] # TODO: parse emission_interval, calculate top 5 vals on that range

# global variables
column_indices=[]
column_names=[]
all_data=[] # store raw data by column
metadata=[] # stores sample name and date
background_data=[]
bg_metadata=[]
first_row=[]

print "data_filename: " + data_filename
print "IA_filename: " + IA_filename
print "emission_interval: " + emission_interval

#column_header = raw_input("enter the column header, for example 'C2FDP': ")

# open and parse the Aqualog 1D raw data
with open(data_filename) as csvFile:
    reader = csv.reader(csvFile)
    
    number_of_rows=0
    for row in reader:
    	if row[0]=="Emission wavelength":
    		first_row=row
    		for index, item in enumerate(first_row):
    			column_indices.append(index)
    			column_names.append(item)
    		#print column_names
    	number_of_rows+=1
    print "number_of_rows: " + str(number_of_rows)
    number_of_columns = len(column_names)
    print "number of columns: " + str(number_of_columns)
    #print column_names[number_of_columns-1]


#load dataframe with header and without header
df = pd.read_csv(data_filename, delimiter=',', header=None)
df_with_header = pd.read_csv(data_filename, delimiter=',')

#iterate over columns to store values of each column in [all_data] and sample name/date in [metadata]
for col in df:
	#metadata.append(df[col])
	full_col = df[col]
	metadata.append(full_col[:2]) #store sample name and date
	all_data.append(full_col[2:]) #store raw data

# print all_data[1395]
# print metadata[1395]

#store background columns in separate list
for index, vals in enumerate(metadata):
	if "BG" in vals[0]:
		background_data.append(all_data[index])
		bg_metadata.append(metadata[index])

print background_data[0]

# for each sample, find the appropriate BG dataset and subtract from sample
bgcorrected=[]
for A_index, A_val in enumerate(metadata):
	for B_index, B_val in enumerate(bg_metadata):
		bg_name=B_val
		print "initial bg_name: " + bg_name
		if "JUNE" in bg_name:
			bg_name=bg_name[:len(bg_name)-2]
			print "bgname: " + bg_name
		else:
			bg_name=

# outfile_name = ''+'.csv'
# #write applicable columns to csv
# with open(outfile_name, 'wb') as myfile:
#     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    
#     wr.writerow(column_names)
#     for row in all_data:
#     	wr.writerow(row)

print "DONE"

