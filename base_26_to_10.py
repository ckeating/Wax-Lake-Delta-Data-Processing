#converts "base 26" numbers (that is, the base 26 Excel uses to denote columns; e.g C or AN or ZRD) to "Human"
import sys

base26num = sys.argv[1]

base10num = 0
alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O","P", "Q", "R", 
	"S", "T", "U", "V", "W", "X", "Y", "Z"]

for ind, digit in enumerate(base26num):
	for index, letter in enumerate(alphabet):
		if digit==letter:
			base10num+= (index+1) * 26**(len(base26num)-ind-1)
	
print base10num