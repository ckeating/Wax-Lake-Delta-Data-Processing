# Author: Colin Keating

# 8/29/17

# INPUT: BG and sample emission spectra (raw data) for a particular excitation wavelength, Aqualog Integration Areas
# COMPUTATION: subtract BG from samples, correct for Raman area, compute peak for user-specified emission interval 
#  note: the interval is inclusive by default
# OUTPUT: corrected sample spectra in csv format

# NOTE: input file must be in same directory as this program and must be saved as Windows CSV

import sys, csv, heapq
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
# check for correct number of args

if len(sys.argv)!=2:
    sys.exit("usage: Python Aqualog_1D_plot.py [filename]")

# get filename 
data_filename=sys.argv[1]
print "data_filename: " + data_filename

# load dataframes (one version of full dataset with header and one without header)
df = pd.read_csv(data_filename, delimiter=',' , header=None)

final_data = df.T.values # transpose the rows/columns so that each sublist stores an individual dataset (e.g. each sublist is one column of data form the original input file)
print "length final_data: " + str(len(final_data))
print "length final_data[0] : " + str(len(final_data[0]))
print "length final_data[1] : " + str(len(final_data[0]))
print "final_data[0][0] should be sample ID: " + str(final_data[0][0])
print "final_data[1][0] should be mean of 5: " + str(final_data[1][0])

# plot max fluorescence vs time for each depth:

plots=[] 	# plots is "3d array" (er, list of list of lists) where outer index is the plot, middle index is the depth, inner index are peak values at that depth
temp_plot_times=[] # temporary list to store times for each depth
temp_plot_values=[] # temp list to store peak fluorescence values for each depth
title=[]
index=1
set_ID=str(final_data[index][0]).split(" ")[0]
while index < len(final_data)-1: # loop through entire final_data list
	if not "BG" in str(final_data[index][0]):
		current=str(final_data[index][0]).split(" ")[0]
		print "current: "+ current
		if set_ID == current:
			temp_plot_values.append(final_data[index][1])
			temp_plot_times.append(final_data[index][3])
		else:
			plots.append([temp_plot_times, temp_plot_values]) #append all samples in the depth to list of plots
			title.append(current)
			temp_plot_values=[] # reset temporary list
			temp_plot_times=[] # reset temporary list
			temp_plot_values.append(final_data[index][1])
			temp_plot_times.append(final_data[index][3])
			set_ID = str(final_data[index][0]).split(" ")[0] # reset set_ID 
	index+=1
	

# plot fluorescence vs time:

# adjust font size
plt.rcParams.update({'font.size': 6})

fig1, axs = plt.subplots(4,7, figsize=(15, 6), facecolor='w', edgecolor='k')
fig1.subplots_adjust(hspace = .5, wspace=.5)
axs = axs.ravel()

for index, plot_values in enumerate(plots):
	#assign correct colors to subplots
	color = title[index][-1]
	if color=="W":
		color="grey"
	elif color=="P":
		color="purple"
	print color
	print plot_values[0]
	axs[index].plot(plot_values[0], plot_values[1], color)
	
	axs[index].set_title(str(title[index]), size=6)	# title
	plt.tick_params(axis='both', which='major', labelsize=6)

	#set x and y range
	axs[index].set_xlim([0, 60])
	#axs[index].set_ylim([0, .25])

fig1.suptitle("Peak Fluorescence vs Time (min)", fontsize=14)




# plot only Colocasia:
# fig2=plt.figure()

# fig2, axs = plt.subplots(4,7, figsize=(15, 6), facecolor='w', edgecolor='k')
# fig2.subplots_adjust(hspace = .5, wspace=.5)
# axs = axs.ravel()

# for index, plot_values in enumerate(plots):
# 	#assign correct colors to subplots
# 	plant_type = title[index][0]
# 	if "JUNE" in title[index]:
# 		plant_type=title[index][5]

# 	color = title[index][-1]
# 	if color=="W":
# 		color="grey"
# 	elif color=="P":
# 		color="purple"
	
# 	if plant_type=="C":
# 		axs[index].plot(plot_values[0], plot_values[1], color)
# 		axs[index].set_title(str(title[index]), size=6)	# title
# 		plt.tick_params(axis='both', which='major', labelsize=6)

# 		#set x and y range
# 		axs[index].set_xlim([0, 60])
# 		#axs[index].set_ylim([0, .25])

# fig2.suptitle("Peak Fluorescence vs Time (min)", fontsize=14)


plt.show()




print "\nDONE"
# END OF SCRIPT